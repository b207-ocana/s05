SELECT customerName FROM customers WHERE country = 'Philippines';

SELECT customerName FROM customers WHERE country = 'USA';

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = 'La Rochelle Gifts';

SELECT productName, MSRP FROM products WHERE productName = 'The Titanic';

SELECT firstName, lastName FROM employees WHERE email = 'jfirrelli@classicmodelcars.com';

SELECT customerName FROM customers WHERE state IS NULL;

SELECT firstName, lastName, email FROM employees WHERE lastName='Patterson' AND firstName = 'Steve';

SELECT customerName, country, creditLimit FROM customers WHERE country != 'USA' AND creditLimit > 3000;

SELECT customerNumber FROM orders WHERE comments LIKE '%DHL%';

SELECT * FROM productlines WHERE textDescription LIKE '%state of the art%';

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers WHERE country = 'USA' OR country = 'France' OR country = 'Canada';

SELECT firstName, lastName, city FROM offices
JOIN employees ON employees.officeCode = offices.officeCode
WHERE city = 'Tokyo';

SELECT customerName FROM employees 
JOIN customers ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE lastName = 'Thompson' AND firstName = 'Leslie';

SELECT productName, quantityInStock FROM products WHERE productLine = 'planes' AND quantityInStock < 1000;

SELECT COUNT(country) FROM customers WHERE country = 'UK';

SELECT customerName, productName FROM customers
JOIN orders ON orders.customerNumber = customers.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode
WHERE customerName = 'Baane Mini Imports';

SELECT employees.lastName, employees.firstName FROM employees 
JOIN employees AS supervisor ON employees.reportsTo = supervisor.employeeNumber
WHERE supervisor.lastName = "Bow" AND supervisor.firstName = "Anthony";

SELECT productName, MAX(MSRP) FROM products;

SELECT productLine, COUNT(*) AS numberOfProducts FROM products
GROUP BY productLine;

SELECT COUNT(*) AS numberOfCancelledOrders FROM orders WHERE status = 'Cancelled';